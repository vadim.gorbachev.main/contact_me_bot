import os

from dotenv import load_dotenv
from aiogram import types
from aiogram.dispatcher.filters.state import State, StatesGroup
from emoji import emojize


load_dotenv()
TOKEN = str(os.getenv('TOKEN'))
ADMIN_ID = int(os.getenv('ADMIN_ID'))


MESSAGES = {
    'START': 'Hi, It\'s my bot. You can contact me by him. Just write a message here '
             'and I will answer as soon as possible.\n\n'
             'Or contact me via email\n <i><b>vadim.gorbachov.main@yandex.ru</b></i>'
             '\n\nThank you :)',
    'WRITE': 'Please, write your message',
    'THANKS': 'Thank you for contacting me!  ' + emojize(':slightly_smiling_face:'),
    'RESUME': 'There is my resume ' + emojize(':clipboard:')
}

markup = types.InlineKeyboardMarkup(
    row_width=1,
    inline_keyboard=[
        [
            types.InlineKeyboardButton(
                text='Write me  ' + emojize(':rocket:'),
                callback_data='write_vadim'
            )
        ],
        [
            types.InlineKeyboardButton(
                text='Get resume  ' + emojize(':clipboard:'),
                callback_data='resume'
            )
        ]
    ]
)
back_markup = types.InlineKeyboardMarkup(
    row_width=1,
    inline_keyboard=[
        [
            types.InlineKeyboardButton(
                text='Back  ' + emojize(':BACK_arrow:'),
                callback_data='start_menu'
            )
        ]
    ]
)


class AnswerState(StatesGroup):
    text = State()
