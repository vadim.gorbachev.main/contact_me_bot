import logging

from aiogram import Dispatcher

import config


async def notify_admin(dp: Dispatcher, message: str = 'Bot launched'):
    try:
        await dp.bot.send_message(config.ADMIN_ID, message)
    except Exception as exc:
        logging.exception(exc)
