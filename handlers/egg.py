from aiogram import types

from app import dp
from middleware.throttling import rate_limit


words = ('люблю вадимчикав', 'вадимчикав', 'люблю', 'Люблю', 'Вадимчикав')


@rate_limit(15, 'wow_handler')
@dp.message_handler(lambda msg: msg.text in words)
async def get_wow(msg: types.Message):
    media = types.MediaGroup()
    media.attach_photo(types.InputFile('files/wow.jpeg'), 'WOW!')
    await msg.answer_media_group(media=media)
    user = msg.from_user.first_name
    if 'юблю' in msg.text:
        if msg.from_user.full_name == 'Элла Салихова':
            user = 'Данечкав'
        await msg.answer(f'а я тебя, {user}')
