from typing import Union

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.builtin import CommandStart, CommandHelp

import config
from admin import notify_admin
from app import bot, dp


@dp.message_handler(CommandStart())
@dp.message_handler(CommandHelp())
@dp.callback_query_handler(lambda callback: callback.data == 'start_menu')
async def get_start(msg_or_cb: Union[types.Message, types.CallbackQuery]):
    if isinstance(msg_or_cb, types.CallbackQuery):
        await msg_or_cb.answer()
    await bot.send_message(
        msg_or_cb.from_user.id,
        config.MESSAGES['START'],
        reply_markup=config.markup
    )


@dp.callback_query_handler(lambda callback: callback.data == 'write_vadim')
async def text_me(callback: types.CallbackQuery):
    await callback.answer()
    await config.AnswerState.text.set()
    await bot.send_message(
        callback.from_user.id,
        config.MESSAGES['WRITE']
    )


@dp.message_handler(state=config.AnswerState.text)
async def get_text(msg: types.Message, state: FSMContext):
    message_from_user = f'Message from user: @{msg.from_user.username}\n' \
                        f'username: {msg.from_user.full_name}\n' \
                        f'user_id: {msg.from_user.id}\n\n' \
                        f'message:\n <i>{msg.text}</i>'
    await notify_admin(dp, message_from_user)
    await state.finish()
    await msg.answer(config.MESSAGES['THANKS'], reply_markup=config.back_markup)
