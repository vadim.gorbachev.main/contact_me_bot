from typing import Union

from aiogram import types

import config
from app import bot, dp
from middleware.throttling import rate_limit


@rate_limit(15, 'resume_handler')
@dp.message_handler(commands=['resume'])
@dp.callback_query_handler(lambda callback: callback.data == 'resume')
async def get_resume(msg_or_cb: Union[types.Message, types.CallbackQuery]):
    if isinstance(msg_or_cb, types.CallbackQuery):
        await msg_or_cb.answer()
    media = types.MediaGroup()
    await bot.send_message(msg_or_cb.from_user.id, 'Just wait a second...')
    media.attach_document(types.InputFile('files/resume_Gorbachev.pdf'))
    await bot.send_media_group(msg_or_cb.from_user.id, media=media)
    await bot.send_message(
        msg_or_cb.from_user.id,
        config.MESSAGES['RESUME'],
        reply_markup=config.back_markup
    )
