import asyncio
import logging

from aiogram import Dispatcher, types, bot
from aiogram.dispatcher import DEFAULT_RATE_LIMIT
from aiogram.dispatcher.handler import CancelHandler, current_handler
from aiogram.dispatcher.middlewares import BaseMiddleware
from aiogram.utils.exceptions import Throttled


def rate_limit(limit: int, key = None):

    def wrap(func):
        setattr(func, 'throttling_rate_limit', limit)
        if key:
            setattr(func, 'throttling_key', key)
        return func

    return wrap


class ThrottlingMIddleware(BaseMiddleware):

    def __init__(self, limit=DEFAULT_RATE_LIMIT, key_prefix='antiflood_'):
        self.rate_limit = limit
        self.prefix = key_prefix
        super(ThrottlingMIddleware, self).__init__()

    @staticmethod
    def get_handler_and_dispatcher():
        try:
            return current_handler.get(), Dispatcher.get_current()
        except Exception as exc:
            logging.exception(f'throttling_midlleware_error: {exc}')

    async def on_process_message(self, msg: types.Message, data: dict):
        handler, dp = self.get_handler_and_dispatcher()

        if handler:
            limit = getattr(handler, 'throttling_rate_limit', self.rate_limit)
            key = getattr(handler, 'throttling_key', f'{self.prefix}_{handler.__name__}')
        else:
            limit = self.rate_limit
            key = f'{self.prefix}_message'

        try:
            await dp.throttle(key, rate=limit)
        except Throttled as exc:
            await self.message_throttled(msg, exc)
            raise CancelHandler()

    async def message_throttled(self, msg: types.Message, throttled: Throttled):
        handler, dp = self.get_handler_and_dispatcher()

        if handler:
            key = getattr(handler, 'throttling_key', f'{self.prefix}_{handler.__name__}')
        else:
            key = f'{self.prefix}_message'

        delta = throttled.rate - throttled.delta
        if throttled.exceeded_count <= 2:
            await msg.answer('Hold on, too many requests :)')
        elif throttled.exceeded_count <= 3:
            await msg.answer('Please stop! Wait a sec, too many requests from you :|')
        await asyncio.sleep(delta)

        thr = await dp.check_key(key)
        if thr.exceeded_count == throttled.exceeded_count:
            await msg.answer('Unlocked')
