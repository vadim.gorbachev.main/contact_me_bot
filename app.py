import logging

from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage

import config
from admin import notify_admin
from middleware.throttling import ThrottlingMIddleware


bot = Bot(token=config.TOKEN, parse_mode=types.ParseMode.HTML)
dp = Dispatcher(bot, storage=MemoryStorage())


async def default_commands(dp: Dispatcher):
    await dp.bot.set_my_commands([
        types.BotCommand('start', 'to the start'),
        types.BotCommand('help', 'same :)'),
        types.BotCommand('resume', 'get my resume')
    ])


async def on_startup(dp: Dispatcher):
    await notify_admin(dp)
    await default_commands(dp)
    dp.middleware.setup(ThrottlingMIddleware())


async def on_shutdown(dp: Dispatcher):
    await notify_admin(dp, 'Bot stop polling')


if __name__ == '__main__':
    from handlers import dp

    logformat = '%(asctime)s - %(message)s'
    dateformat = '%d-%b-%y %H:%M:%S'
    logging.basicConfig(format=logformat, datefmt=dateformat, level=logging.INFO)

    executor.start_polling(dp, on_startup=on_startup, on_shutdown=on_shutdown, skip_updates=True)
